
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SOPFY | Ledesma</title>

    <!-- Font Icon -->
    <?= $this->Html->css('/webroot/wizard/fonts/material-icon/css/material-design-iconic-font.min.css') ?>
    <!-- Main css -->
    <?= $this->Html->css('/webroot/wizard/css/style.css') ?>

    <script src="https://use.fontawesome.com/0c65625677.js"></script>
    <link href="https://use.fontawesome.com/0c65625677.css" media="all" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">

</head>
<body>

<div class="main">

    <div class="container">
        <form method="POST" id="registro" class="registro" action="submit.php" enctype="multipart/form-data">

            <h3>Confirmación</h3>
            <fieldset style="border-radius: 25px;
                             border: 2px solid #d3d3d3;
                             padding: 20px;
                             width: fit-content;
                             height: fit-content;">
                <div style="text-align: center; margin-bottom: 10px; margin-top:30px; color:#0b96d1;">
                    <i class="fa fa-check-circle fa-7x"></i>

                </div>
                <h2>Punto registrado correctamente. Gracias. </h2><br>
                <h2>Redirigiendo a Sopfy.com...</h2>
            </fieldset>
        </form>
        <div style="text-align: center; padding-top: 10px">
            <?=$this->Html->image('/webroot/wizard/images/sopfy.jpg', ['height'=> '30']);?>
        </div>
    </div>

</div>

<!-- JS -->
<?= $this->Html->script('/webroot/wizard/vendor/jquery/jquery.min.js') ?>
<?= $this->Html->script('/webroot/wizard/vendor/jquery-validation/dist/jquery.validate2.min.js') ?>
<?= $this->Html->script('/webroot/wizard/vendor/jquery-validation/dist/additional-methods.min.js') ?>
<?= $this->Html->script('/webroot/wizard/vendor/jquery-steps/jquery.steps.min.js') ?>
</body>
</html>
<script>
    $(document).ready(function () {
        // Handler for .ready() called.
        window.setTimeout(function () {
            location.href = "https://sopfy.com/";
        }, 3000);
    });
</script>
