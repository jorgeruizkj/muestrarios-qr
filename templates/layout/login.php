<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro POP <?= ucwords($marca) ?></title>

    <!-- Font Icon -->
    <?= $this->Html->css('/webroot/wizard/fonts/material-icon/css/material-design-iconic-font.min.css') ?>
    <!-- Main css -->
    <?= $this->Html->css('/webroot/wizard/css/'.strtolower($marca).'.css') ?>
    <!-- fontawesome cdn -->
    <script src="https://use.fontawesome.com/0c65625677.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- jQuery Modal -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

</head>
<style>
    .modal {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .video {
        max-width:100%;
        max-height:100%;
    }

    @media screen and (max-width: 352px){

        #registro-t-1, #registro-t-2, #registro-t-3 {
            margin-top:23px;
        }

    }

    a.btn{
        color: #fff;
        background: #00D7F9;
        padding: .5rem 1rem;
        display: inline-block;
        border-radius: 4px;
        transition-duration: .25s;
        border: none;
        font-size: 14px;
        text-decoration: none;
    }

    .buttons{
        text-align:center;
    }

    .imgModal{
        width:400px;
        height: 600px;
    }



</style>
<body>

<div class="main">

    <div class="container" style="display:none;">
        <!-- Modal HTML embedded directly into document -->
        <div id="ex1" class="modal" style="height:fit-content; width:fit-content;">
            <?= $this->Html->media('/webroot/wizard/images/geolocation_notice.mp4', ['autoplay', 'muted', 'playsinline', 'class' => 'video']) ?>
            <p> Debe habilitar su ubicación para continuar </p>
        </div>

        <div id="ex2" class="modal" style="height:fit-content; width:fit-content;">
            <p> ¿Ya ha escaneado este producto el dia de hoy? </p>
            <div class="buttons">
                <a id="yes" class="btn" href="#" rel="modal:close"> Sí </a>
                <a id="no" class="btn" href="#" rel="modal:close"> No </a>
            </div>
        </div>

        <div id="ex3" class="modal" style="max-height:600px; max-width:600px; width:fit-content; ">
            <p> Si encuentra el producto entre estas imágenes, clickee la correspondiente. </p>
            <div class="buttons" id="ex3images" style=" width:400px;
        height: 400px; overflow-y:scroll;">

            </div>
        </div>

        <?=$this->Form->create(null, ['action' => $this->Url->build(['controller' => 'Users', 'action' => 'add', $marca, $product_code]),
            'id' => 'registro', 'class' => 'registro', 'enctype' => 'multipart/form-data']) ?>
            <input type="hidden" name="_csrfToken" autocomplete="off" value="<?= $this->request->getAttribute('csrfToken'); ?>"/>
            <h3>
                Datos Personales
            </h3>
            <fieldset>
                <div style="text-align: center; padding-top:10px;">
                    <?=$this->Html->image('/webroot/wizard/images/'.strtolower($marca).'.jpg', ['height'=> '100', 'style' => 'border-radius: 15px;']);?>
                </div>
                <h2 style="padding-top: 5px">Ingrese sus datos</h2>

                <div class="form-group">
                    <input class="A" type="text" name="name" id="nombre" placeholder="Nombre" required/>
                    <input type="hidden" name="latitude" id="latitud"/>
                    <input type="hidden" name="longitude" id="longitud"/>
                </div>
                <div class="form-group">
                    <input class="A" type="text" name="surname" id="apellido" placeholder="Apellido" required/>
                </div>
                <div class="form-group">
                    <input class="A" type="email" name="mail" id="email" placeholder="Mail" required/>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="tyc" id="tyc" value="1" style="width: auto!important; height: auto; display: initial;" required/> Acepto terminos y condiciones
                </div>

                <input class="text" type="hidden" name="product_code"  id="product_code" value="<?= $product_code ?>">
                <input class="text" type="hidden" name="location_id"   id="location_id" value="">
                <input type="hidden" name="brand_id" id="brand_id"     value="<?= $brand_id ?>"/>
                <input type="hidden" name="product_id" id="product_id" value="<?=$product_id?>" >
                <input type="hidden" name="unique_id" id="unique_id"   value="" >

            </fieldset>

            <h3>
                Comercio
            </h3>
            <fieldset>
                <h2>Complete los campos</h2>
                <label>¿Cual es tu rol?</label>
                <div class="form-group">
                    <select class="A" name="role" id="rol" required style="box-sizing: border-box;
                            background: transparent;
                            border: none;
                            font-size: 13px;
                            color: #222;
                            padding: 15px 20px;
                            border: 1px solid #ebebeb;
                            font-family: 'Poppins';
                            height: 50px;
                            font-weight: 500;
                            border-radius: 5px;
                            -moz-border-radius: 5px;
                            -webkit-border-radius: 5px;
                            -o-border-radius: 5px;
                            -ms-border-radius: 5px; display: block; width: 100%">
                        <option disabled selected></option>
                        <option value="distribuidor">Distribuidor</option>
                        <option value="repositor">Repositor</option>
                        <option value="minorista">Minorista</option>
                        <option value="comercial">Comercial</option>
                    </select>
                </div>
                <label>¿Que tipo de comercio?</label>
                <div class="form-group">
                    <select name="type" id="comercio" required style="box-sizing: border-box;
                            background: transparent;
                            border: none;
                            font-size: 13px;
                            color: #222;
                            padding: 15px 20px;
                            border: 1px solid #ebebeb;
                            font-family: 'Poppins';
                            height: 50px;
                            font-weight: 500;
                            border-radius: 5px;
                            -moz-border-radius: 5px;
                            -webkit-border-radius: 5px;
                            -o-border-radius: 5px;
                            -ms-border-radius: 5px; display: block; width: 100%">
                        <option disabled selected></option>
                        <option value="Hiper-Supermercado">Hiper-Supermercado</option>
                        <option value="Minimercado">Minimercado</option>
                        <option value="PDV Mayorista">PDV Mayorista</option>
                        <option value="Comercio cercania">Comercio cercanía</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="file-upload" class="custom-file-upload">
                        <i class="fa fa-camera"></i> Subir foto
                    </label>
                    <input id="file-upload" style="display:none;" type="file" accept="image/*" capture="camera" name="foto" required/>
                    <i class="fas fa-check fa-2x checkfoto" style="display:none; color:darkgreen; margin-left:10px;"></i>
                    <button type="submit" style="display:none;"></button>
                </div>

            </fieldset>

            <h3>Confirmación</h3>
            <fieldset>
                <div style="text-align: center; margin-top:30px; color:#0b96d1;">
                    <br>
                    <img id="imgpreview" style="display:none;" width="200px" height="400px" src="#" alt="your image" />
                    <div class="form-group">
                        <label for="file-upload" class="custom-file-upload">
                            <i class="fa fa-camera"></i> Cambiar imagen
                        </label>
                        <input id="file-upload" style="display:none;" type="file" accept="image/*" capture="camera" name="foto2">
<!--                        <i class="fas fa-check fa-2x checkfoto" style="display:none; color:darkgreen; margin-left:10px;"></i>-->
                    </div>                    <br>

                </div>
                <h2 style="padding-top:0px;">Formulario completado.<br/> Haga clic en enviar para finalizar.</h2>
            </fieldset>
<!--        </form>-->
        <?= $this->Form->end() ?>
        <div style="text-align: center; padding-top: 10px">
            <div id="leyenda" style="display:none;">
                <h2> Enviando datos... </h2>
            </div>
            <?=$this->Html->image('/webroot/wizard/images/sopfy.jpg', ['height'=> '30']);?>
        </div>
    </div>

    <!-- JS -->
    <?= $this->Html->script('/webroot/wizard/vendor/jquery/jquery.min.js') ?>
    <?= $this->Html->script('/webroot/wizard/vendor/jquery-validation/dist/jquery.validate2.min.js') ?>
    <?= $this->Html->script('/webroot/wizard/vendor/jquery-validation/dist/additional-methods.min.js') ?>
    <?= $this->Html->script('/webroot/wizard/vendor/jquery-steps/jquery.steps.min.js') ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

</body>
<script>

    //variable que contiene la fecha corriente
    var hoy = new Date().toISOString().substring(0, 10);
    var location_id; //??
    var product_id = $('#product_id').val();
    var idCheck = 0;





    function setCookie(visita, guardada) {
        // Encode value in order to escape semicolons, commas, and whitespace
        var cookie = visita + "=" + guardada;
        document.cookie = cookie;
        console.log(cookie);
    }

    function getCookie(visita) {
        // Split cookie string and get all individual name=value pairs in an array
        var cookieArr = document.cookie.split(";");

        // Loop through the array elements
        for(var i = 0; i < cookieArr.length; i++) {
            var cookiePair = cookieArr[i].split("=");

            /* Removing whitespace at the beginning of the cookie name
             and compare it with the given string */
            if(visita == cookiePair[0].trim()) {
                // Decode the cookie value and return
                return decodeURIComponent(cookiePair[1]);
            }
        }

        // Return null if not found
        return null;
    }

    //GEOLOCALIZACIÓN -> SOLO FUNCIONA CON HTTPS
    function getLocation () {
        if (navigator.geolocation) { //chequea si el API de geolocalizacion esta disponible
//            handlePermission();
            //consigue lat y long
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            $('#ex1').modal('show');
        }
    }

    function createImgModal(img){
        idCheck++;
        var imgRoute   = "<?=$this->Url->Build("/webroot/uploads/registration/")?>";
        console.log(img['unique_id']);
        var imgPath = imgRoute + img['route'];
        $('#ex3images').append($('<button class="btnImage" data-id="' + img['unique_id'] +'" >' + '<img src="' + imgPath + '" class="imgModal" >' + '</button> <br>'));


    }

    function showError(error) {
        $('#ex1').modal('show');
        $("#ex1").modal({
            escapeClose: false,
            clickClose: false,
            showClose: false
        });

    }

    function showPosition(position) {


        var lat = position.coords.latitude;
        var long = position.coords.longitude;
        var brand_id = $('#brand_id').val();
        var product_id = $('#product_id').val();
//        var product_code = $('')
        if(lat === 0 || long === 0 || lat === undefined || long === undefined  ){
            alert('Debe permitir la geolocalización para registrar el punto.');
            return false;
        }

        //si la geolocación funciona, muestro el form
        $('.container').show();

        $('#latitud').val(position.coords.latitude);
        $('#longitud').val(position.coords.longitude);

        var product_code = $('#product_code').val();

        //COOKIE CHECK: me fijo si hay una locacion registrada con estas coordenadas
        $.ajax({
            url: '<?=$this->Url->build(['controller' => 'Users', 'action' => 'getLocationId'])?>',
            type: 'post',
            headers: {
                'X-CSRF-Token':
                <?= json_encode($this->request->getAttribute('csrfToken')); ?>
            },
            data: {
                longitud: long, latitud: lat, product_code: product_code, brand_id:brand_id
            },
            success: function (data) {
                console.log('getLocId ' + data);
                //on success, me fijo si existe una cookie grabada en el dispositivo para el día de hoy y el product code
                //correspondiente
//                    console.log(data);
//                    console.log(getCookie('visita_' + data + '_' + hoy));
                var chk = getCookie('visita_' + data + '_' + hoy);
                if (chk == null) {
                    //si no hay tal cookie, voy a savevisita()
                    $.ajax({
                        url: '<?=$this->Url->build(['controller' => 'Users', "action" => 'savevisita']);?>',
                        type: 'post',
                        headers: {
                            'X-CSRF-Token':
                            <?= json_encode($this->request->getAttribute('csrfToken')); ?>
                        },
                        data: {
                            longitud: long, latitud: lat, product_code: product_code, brand_id: brand_id
                        },
                        dataType: "json",
                        success: function (e) {
                            console.log(e);
                            var cookieData = JSON.stringify(e);
                            console.log(product_id);
                            console.log(e.id);
                            //grabo la cookie con los datos que tenga y el nombre correspondiente para buscarla en
                            //el cookie check
                            setCookie('visita_' + e.id + '_' + hoy, cookieData);
                            $('#location_id').val(e.id);
                            $('#comercio').val(e.type);

                            $.ajax({
                                url: '<?=$this->Url->build(['controller' => 'Users', "action" => 'getImages']);?>',
                                type: 'post',
                                headers: {
                                    'X-CSRF-Token':
                                    <?= json_encode($this->request->getAttribute('csrfToken')); ?>
                                },
                                data: {
                                    product_id: product_id, location_id: e.id
                                },
                                dataType: "json",
                                success: function (e) {
//                                    $('.container').hide();
                                    console.log(e);
                                    if(e.length > 0){
                                        e.forEach(createImgModal);
                                        $('#ex3').modal('show');
                                        $(".btnImage").click(function(){
                                            console.log($(this).attr('data-id'));
                                            $("#unique_id").val($(this).attr('data-id'));
                                            $('#ex3').modal('hide');
//                                            $('.container').show();

                                        });
                                    }
                                },
                                error: function (e) {
                                    console.log(e + "error getImages");
                                }
                            });


                        },
                        error: function (e) {
                            console.log(e + "error savevisita");
                        }
                    });
                } else {
                    //muestro el modal de confirmación de registro de locación, le quito los cierres
                    $('#ex2').modal('show');
                    $("#ex2").modal({
                        showClose: false,
                        escapeClose:false,
                        clickClose:false
                    });
                    $('#yes, #no').click(function () {
                        $('#ex2').modal('hide');
                        if (this.id === 'yes') {
                            var datos = JSON.parse(chk);
                            $('#location_id').val(datos['id']);
                            console.log($('#location_id').val());
                            console.log('cookie existente, save visita innecesario');
                        } else if (this.id === 'no') {
                            $.ajax({
                                url: '<?=$this->Url->build(['controller' => 'Users', "action" => 'savevisita']);?>',
                                type: 'post',
                                headers: {
                                    'X-CSRF-Token':
                                    <?= json_encode($this->request->getAttribute('csrfToken')); ?>
                                },
                                data: {
                                    longitud: long, latitud: lat, product_code: product_code, brand_id: brand_id
                                },
                                dataType: "json",
                                success: function (e) {
                                    console.log(e);
                                    console.log($('#product_id').val());
                                    console.log(e.id);
                                    var cookieData = JSON.stringify(e);
                                    //grabo la cookie con los datos que tenga y el nombre correspondiente para buscarla en
                                    //el cookie check
                                    setCookie('visita_' + e.id + '_' + hoy, cookieData);
                                    $('#location_id').val(e.id);
                                    $('#comercio').val(e.type);

                                    $.ajax({
                                        url: '<?=$this->Url->build(['controller' => 'Users', "action" => 'getImages']);?>',
                                        type: 'post',
                                        headers: {
                                            'X-CSRF-Token':
                                            <?= json_encode($this->request->getAttribute('csrfToken')); ?>
                                        },
                                        data: {
                                            product_id: product_id, location_id: e.id
                                        },
                                        dataType: "json",
                                        success: function (e) {
//                                            $('.container').hide();
                                            console.log(e);
                                            if(e.length > 0){
                                                e.forEach(createImgModal);
                                                $('#ex3').modal('show');
                                                $(".btnImage").click(function(){
                                                    console.log($(this).attr('data-id'));
                                                    $("#unique_id").val($(this).attr('data-id'));

//                                                    $('.container').show();

                                                });
                                            }
                                         },
                                        error: function (e) {
                                            console.log(e + "error getImages");
                                        }
                                    });

                                },
                                error: function (e) {
                                    console.log(e + "error savevisita");
                                }
                            });
                        }
                    });

                }


            },
            error: function (data) {
                console.log(data + "error getLocationId");
            }
        });



    }


    //function para preview de imagen
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imgpreview').attr('src', e.target.result);
                $('#imgpreview').attr('style','max-width:200px; max-height:400px; margin-bottom:10px width: auto; height: auto;');
//                    $('#imgpreview').attr('style','text-align:center; display: block; max-width:230px; max-height:95px; width: auto; height: auto;');
                $('#file-upload').val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }






    $( document ).ready(function() { //carga la pagina



        //disparo getLocation() (que contiene a showPosition())
        getLocation();

        //check cookie de datos de usuario
        //si la encuentro, lleno el formulario con los datos que contiene
        if(getCookie('datos_usuario')){
            console.log('cookie found!');
            var datos = JSON.parse(getCookie('datos_usuario'));
            //a cada input del primer paso le asigno el value correspondiente
            $(".A").each(function () {
                $(this).val(datos[this.id]);
            });
        }

        //image upload
        $("#file-upload, #file-upload2").change(function() {
            readURL(this);
            if($('#file-upload').val().length > 0){
                $('.checkfoto').show();
            }
        });

        //cuando capturo el unique_id, cierro el modal (??)
        $("#unique_id").change(function(){
            $('#ex3').modal('hide');
        });

    });
</script>
</html>
<?= $this->Html->script('/webroot/wizard/js/main.js') ?>

