<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LocationsUser $locationsUser
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Locations User'), ['action' => 'edit', $locationsUser->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Locations User'), ['action' => 'delete', $locationsUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $locationsUser->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Locations Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Locations User'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="locationsUsers view content">
            <h3><?= h($locationsUser->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Location') ?></th>
                    <td><?= $locationsUser->has('location') ? $this->Html->link($locationsUser->location->name, ['controller' => 'Locations', 'action' => 'view', $locationsUser->location->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $locationsUser->has('user') ? $this->Html->link($locationsUser->user->name, ['controller' => 'Users', 'action' => 'view', $locationsUser->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Product') ?></th>
                    <td><?= $locationsUser->has('product') ? $this->Html->link($locationsUser->product->name, ['controller' => 'Products', 'action' => 'view', $locationsUser->product->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($locationsUser->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Visit') ?></th>
                    <td><?= $this->Number->format($locationsUser->visit) ?></td>
                </tr>
                <tr>
                    <th><?= __('Signin') ?></th>
                    <td><?= $this->Number->format($locationsUser->signin) ?></td>
                </tr>
                <tr>
                    <th><?= __('Accept Terms') ?></th>
                    <td><?= $this->Number->format($locationsUser->accept_terms) ?></td>
                </tr>
                <tr>
                    <th><?= __('Date') ?></th>
                    <td><?= h($locationsUser->date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
