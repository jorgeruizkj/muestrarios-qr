<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LocationsUser[]|\Cake\Collection\CollectionInterface $locationsUsers
 */
?>
<div class="locationsUsers index content">
    <?= $this->Html->link(__('New Locations User'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Locations Users') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('location_id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('product_id') ?></th>
                    <th><?= $this->Paginator->sort('visit') ?></th>
                    <th><?= $this->Paginator->sort('signin') ?></th>
                    <th><?= $this->Paginator->sort('date') ?></th>
                    <th><?= $this->Paginator->sort('accept_terms') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($locationsUsers as $locationsUser): ?>
                <tr>
                    <td><?= $this->Number->format($locationsUser->id) ?></td>
                    <td><?= $locationsUser->has('location') ? $this->Html->link($locationsUser->location->name, ['controller' => 'Locations', 'action' => 'view', $locationsUser->location->id]) : '' ?></td>
                    <td><?= $locationsUser->has('user') ? $this->Html->link($locationsUser->user->name, ['controller' => 'Users', 'action' => 'view', $locationsUser->user->id]) : '' ?></td>
                    <td><?= $locationsUser->has('product') ? $this->Html->link($locationsUser->product->name, ['controller' => 'Products', 'action' => 'view', $locationsUser->product->id]) : '' ?></td>
                    <td><?= $this->Number->format($locationsUser->visit) ?></td>
                    <td><?= $this->Number->format($locationsUser->signin) ?></td>
                    <td><?= h($locationsUser->date) ?></td>
                    <td><?= $this->Number->format($locationsUser->accept_terms) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $locationsUser->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $locationsUser->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $locationsUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $locationsUser->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
