
<div class="form" style="text-align:center;">

    <?= $this->Form->create(null, ['url' => ['controller' => 'Clients', 'action' => 'login'], 'id' => 'registro']) ?>
    <fieldset>
        <legend style="font-size: 200%;"><?= __('¡Bienvenido!') ?></legend>
        <label style="font-size: 125%;"> Usuario </label>
        <?= $this->Form->control('username', ['label' => false, 'style' => 'font-size: 100%;', 'required']) ?>
        <label style="font-size: 125%;"> Contraseña</label>
        <?= $this->Form->control('password', ['label' => false, 'style' => 'font-size: 100%;', 'required']) ?>
    </fieldset>
    <?= $this->Form->button(__('Ingresar'), ['style' => 'margin-top:10px; font-size: 125%;', 'id' => 'btn']); ?>
    <?= $this->Form->end() ?>
</div>