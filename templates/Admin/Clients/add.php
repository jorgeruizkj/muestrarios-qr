<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Clients'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="clients form content">
            <?= $this->Form->create($client) ?>
            <fieldset>
                <legend><?= __('Add Client') ?></legend>
                <?php
                echo $this->Form->control('name');
                echo $this->Form->control('surname');
                echo $this->Form->control('username');
                echo $this->Form->control('password');
                echo $this->Form->control('dni');
                echo $this->Form->control('mail');
                echo $this->Form->control('brand_id', ['options' => $brands, 'empty' => true]);
                echo $this->Form->control('type');
                echo $this->Form->control('accept_terms');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
