<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Sopfy | Administrador | Login </title>

    <!-- Font Icon -->
    <?= $this->Html->css('/webroot/wizard/fonts/material-icon/css/material-design-iconic-font.min.css') ?>
    <!-- Main css -->
    <?= $this->Html->css('/webroot/wizard/css/style.css') ?>
    <!-- fontawesome cdn -->
    <script src="https://use.fontawesome.com/0c65625677.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
</head>

<body>

<div class="main"style="display: flex;
  align-items: center;
  justify-content: center;">
    <div>
        <div class="container" style="display:flex;align-items: center;
                                      justify-content: center;flex-direction: column;;">
            <?=$this->Html->image('/webroot/wizard/images/sopfy.jpg', ['width'=> '70%', 'height' => '70%']);?>
            <?= $this->fetch('content')?>

        </div>
    </div>

</div>

    <!-- JS -->
    <?= $this->Html->script('/webroot/wizard/vendor/jquery/jquery.min.js') ?>
    <?= $this->Html->script('/webroot/wizard/vendor/jquery-validation/dist/jquery.validate2.min.js') ?>
    <?= $this->Html->script('/webroot/wizard/vendor/jquery-validation/dist/additional-methods.min.js') ?>
    <?= $this->Html->script('/webroot/wizard/vendor/jquery-steps/jquery.steps.min.js') ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

</body>
</html>


