<!DOCTYPE html>
<html>
<head>
    <!-- Basic Page Info -->
    <meta charset="utf-8">
    <title>Sopfy | <?=$title?></title>

    <!-- Site favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/webroot/deskapp2/vendors/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/webroot/deskapp2/vendors/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/webroot/deskapp2/vendors/images/favicon-16x16.png">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <!-- CSS -->
    <?= $this->Html->css('/webroot/deskapp2/vendors/styles/core.css'); ?>
    <?= $this->Html->css('/webroot/deskapp2/vendors/styles/icon-font.min.css'); ?>
    <?= $this->Html->css('/webroot/deskapp2/src/plugins/datatables/css/dataTables.bootstrap4.min.css'); ?>
    <?= $this->Html->css('/webroot/deskapp2/src/plugins/datatables/css/responsive.bootstrap4.min.css'); ?>
    <?= $this->Html->css('/webroot/deskapp2/vendors/styles/style.css'); ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>

    <?= $this->Html->script("/webroot/deskapp2/vendors/scripts/core.js"); ?>
    <?= $this->Html->script("/webroot/deskapp2/vendors/scripts/script.min.js"); ?>

    
</head>
<body>
<div class="pre-loader">
    <div class="pre-loader-box">
        <div class="loader-logo"><img src="<?=$this->Url->Build("/webroot/deskapp2/src/images/layout/sopfy.jpeg")?>" height="40" alt=""></div>
        <div class='loader-progress' id="progress_div">
            <div class='bar' id='bar1'></div>
        </div>
        <div class='percent' id='percent1'>0%</div>
        <div class="loading-text">
            Cargando...
        </div>
    </div>
</div>

<div class="header">
    <div class="header-left">
        <div class="menu-icon dw dw-menu"></div>
    </div>
    <div class="header-right">
       <div class="user-info-dropdown">
            <div class="dropdown">
                <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon">
							<img src="<?=$this->Url->Build("/webroot/deskapp2/src/images/layout/account.png")?>" alt="">
						</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                    <!--<a class="dropdown-item" href="profile.html"><i class="dw dw-user1"></i> Profile</a>
                    <a class="dropdown-item" href="profile.html"><i class="dw dw-settings2"></i> Configuraciones</a>
                    -->
                    <a class="dropdown-item" href="faq.html"><i class="dw dw-help"></i> Ayuda</a>
                    <a class="dropdown-item" href="<?=$this->Url->Build(['controller'=>'Clients','action' => 'logout']);?>"><i class="dw dw-logout"></i> Salir</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="left-side-bar">
    <div class="brand-logo">
        <a href="<?=$this->Url->Build(["controller"=>"Main", "action"=> 'index']);?>">
            <img src="/webroot/deskapp2/vendors/images/deskapp-logo.svg" alt="" class="dark-logo">
            <img src="/webroot/deskapp2/vendors/images/deskapp-logo-white.svg" alt="" class="light-logo">
        </a>
        <div class="close-sidebar" data-toggle="left-sidebar-close">
            <i class="ion-close-round"></i>
        </div>
    </div>
    <div class="menu-block customscroll">
        <div class="sidebar-menu">
            <ul id="accordion-menu">
                <li class="dropdown">
                    <a href="<?=$this->Url->Build(["controller"=>"Main", "action"=> 'index']);?>" class="dropdown-toggle no-arrow">
                        <span></span><span class="mtext">Inicio</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?=$this->Url->Build(["controller"=>"Main", "action"=> 'index']);?>" class="dropdown-toggle no-arrow">
                        <span></span><span class="mtext">Campañas</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?=$this->Url->Build(["controller"=>"Main", "action"=> 'index']);?>" class="dropdown-toggle no-arrow">
                        <span></span><span class="mtext">Ubicaciones</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?=$this->Url->Build(["controller"=>"Main", "action"=> 'index']);?>" class="dropdown-toggle no-arrow">
                        <span></span><span class="mtext">Productores</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?=$this->Url->Build(["controller"=>"Main", "action"=> 'index']);?>" class="dropdown-toggle no-arrow">
                        <span></span><span class="mtext">Gestores</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="mobile-menu-overlay"></div>

<?= $this->fetch('content') ?>
<!-- js -->
<?= $this->Html->script("/webroot/deskapp2/vendors/scripts/process.js"); ?>
<?= $this->Html->script("/webroot/deskapp2/vendors/scripts/layout-settings.js"); ?>
<?= $this->Html->script("/webroot/deskapp2/src/plugins/apexcharts/apexcharts.min.js"); ?>
<?= $this->Html->script("/webroot/deskapp2/src/plugins/datatables/js/jquery.dataTables.min.js"); ?>
<?= $this->Html->script("/webroot/deskapp2/src/plugins/datatables/js/dataTables.bootstrap4.min.js"); ?>
<?= $this->Html->script("/webroot/deskapp2/src/plugins/datatables/js/dataTables.responsive.min.js"); ?>
<?= $this->Html->script("/webroot/deskapp2/src/plugins/datatables/js/responsive.bootstrap4.min.js"); ?>

<script>

     $('document').ready(function(){
        // Percentage Increment Animation

     });
</script>
</body>
</html>