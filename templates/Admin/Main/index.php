<div class="main-container">
    <div class="pd-ltr-20">
        <div class="card-box pd-20 height-50-p mb-30">
            <div class="row align-items-center">
                <div class="col-md-10">
                    <h4 class="font-20 weight-500 mb-10 text-capitalize">
                        ¡Bienvenido<div class="weight-600 font-20 text-light-blue" style="display: initial;"><?=($brand==null)?'':' '.$brand?></div>!
                    </h4>
                    <p class="font-18">Visibiliza la implementación de tu campaña en tiempo real. Aquí podrás hacer un seguimiento y trazar todos tus productos promocionales.</p>
                </div>
                <div class="col-md-2">
                    <img src="<?=$this->Url->Build("/webroot/uploads/brands/2.jpg")?>" style="max-height: 150px; border-radius: 5px;" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 mb-30">
                <div class="card-box height-100-p widget-style1">
                    <div class="d-flex flex-wrap align-items-center">
                        <div class="progress-data">
                            <div id="active_campaigns"></div>
                        </div>
                        <div class="widget-data">
                            <div class="h4 mb-0"><?=$active_campaigns?></div>
                            <div class="weight-600 font-14">Campañas Activas</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 mb-30">
                <div class="card-box height-100-p widget-style1">
                    <div class="d-flex flex-wrap align-items-center">
                        <div class="progress-data">
                            <div id="active_products"></div>
                        </div>
                        <div class="widget-data">
                            <div class="h4 mb-0"><?=$active_products?></div>
                            <div class="weight-600 font-14">Productos Activos</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 mb-30">
                <div class="card-box height-100-p widget-style1">
                    <div class="d-flex flex-wrap align-items-center">
                        <div class="progress-data">
                            <div id="active_users"></div>
                        </div>
                        <div class="widget-data">
                            <div class="h4 mb-0"><?=$active_users?></div>
                            <div class="weight-600 font-14">Gestores Activos</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 mb-30">
                <div class="card-box height-100-p widget-style1">
                    <div class="d-flex flex-wrap align-items-center pd-20">
                        <div class="width-100">
                            <div class="h4 mb-0"><?=$active_locations?></div>
                            <div class="weight-600 font-14">Ubicaciones Activas</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 mb-30">
                <div class="card-box height-100-p widget-style1">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h4 class="card-label">Ubicaciones en últimos <?=count($locations_dates)?> días</h4>
                            </div>
                        </div>
                        <div class="bg-white pd-20 card-box mb-30">
                            <div id="ubicaciones_chart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 mb-30">
                <div class="card-box height--p widget-style1">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h4 class="card-label">Mapa de ubicaciones</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="map" style="height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-box mb-30">
            <h2 class="h4 pd-20">Mis campañas</h2>
            <table id="campaign-table" class="table nowrap">
                <thead>
                <tr>
                    <?=($brand==null)?'':'<th class="table-plus datatable-nosort">Marca</th>'?>
                    <th>Nombre</th>
                    <th colspan="2">Productos</th>
                    <th>Activación</th>
                    <th>Ciclo de vida</th>
                    <th class="datatable-nosort">Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($campaigns  as $campaign) {
                    if(count($campaign->products)>0) { ?>
                    <tr>
                        <?=($brand==null)?'':
                        '<td class="table-plus">'
                            .$campaign->brand->name.
                        '</td>'?>
                        <td>
                            <h5 class="font-16"><?=$campaign->name?></h5>
                        </td>
                        <td colspan="2"></td>
                        <td>
                            <div style="border: 1px solid green;">
                                <div style="background-color: green; height:10px;width:<?=$campaign->activation?>%"></div>
                            </div>
                            <?=$campaign->activation?>%</td>
                        <td><?=(date_format($campaign->created, 'd/m/Y'))?><?=($campaign->deadline!=null?" / ". (date_format($campaign->deadline, 'd/m/Y')):"");?></td>
                        <td>
                        </td>
                    </tr>
                    <?php foreach ($campaign->products as $product) {

                        $locationsId = []; // auxiliar para buscar si no tengo la locacion ya
                        foreach ($product->locations_history AS $history){
                            // si no la encuentro la agrego al array y ademas lo contabilizo
                            if(!in_array($history->location_id,$locationsId)){
                                $locationsId[] = $history->location_id;
                            }
                        }

                        $percent = round((intval($product->activation_count) * 100) / intval($product->quantity),2); ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="pd-0">
                                <img src="<?=$this->Url->Build("/webroot/uploads/products/".$product->img_path)?>" style="max-height: 40px;" />
                            </td>
                            <td class="table-plus">
                                <a href="<?=$this->Url->Build(["controller" => "Products", "action" => "View", $product->id]);?>"><?=$product->name?> (<?=$product->code?>)</a>
                            </td>
                            <td>
                                <div style="border: 1px solid green;">
                                    <div style="background-color: green; height:10px;width:<?=$percent?>%"></div>
                                </div>
                                <h5 class="font-16"><?=$percent?>% (<?=intval($product->activation_count)?>/<?=$product->quantity?>)</h5>
                            </td>
                            <td></td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="dw dw-more"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                        <a class="dropdown-item" href="<?=$this->Url->Build(["controller" => "Products", "action" => "View", $product->id]);?>"><i class="dw dw-eye"></i> Ver</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="footer-wrap pd-20 mb-20 card-box">
        Sopfy.com - © <?=date("Y");?> - Todos los derechos reservados
    </div>
</div>
<?= $this->Html->script("/webroot/deskapp2/src/plugins/apexcharts/apexcharts.min.js"); ?>
<script>
    var options = {
        series: [<?=$active_campaigns_percent?>],
        grid: {
            padding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
        },
        chart: {
            height: 100,
            width: 70,
            type: 'radialBar',
        },
        plotOptions: {
            radialBar: {
                hollow: {
                    size: '50%',
                },
                dataLabels: {
                    name: {
                        show: false,
                        color: '#fff'
                    },
                    value: {
                        show: true,
                        color: '#333',
                        offsetY: 5,
                        fontSize: '15px'
                    }
                }
            }
        },
        colors: ['#ecf0f4'],
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'diagonal1',
                shadeIntensity: 0.8,
                gradientToColors: ['#1b00ff'],
                inverseColors: false,
                opacityFrom: [1, 0.2],
                opacityTo: 1,
                stops: [0, 100],
            }
        },
        states: {
            normal: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
            hover: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
            active: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
        }
    };

    var options2 = {
        series: [<?=$active_products_percent?>],
        grid: {
            padding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
        },
        chart: {
            height: 100,
            width: 70,
            type: 'radialBar',
        },
        plotOptions: {
            radialBar: {
                hollow: {
                    size: '50%',
                },
                dataLabels: {
                    name: {
                        show: false,
                        color: '#fff'
                    },
                    value: {
                        show: true,
                        color: '#333',
                        offsetY: 5,
                        fontSize: '15px'
                    }
                }
            }
        },
        colors: ['#ecf0f4'],
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'diagonal1',
                shadeIntensity: 1,
                gradientToColors: ['#009688'],
                inverseColors: false,
                opacityFrom: [1, 0.2],
                opacityTo: 1,
                stops: [0, 100],
            }
        },
        states: {
            normal: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
            hover: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
            active: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
        }
    };


    var options3 = {
        series: [<?=$active_locations_percent?>],
        grid: {
            padding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
        },
        chart: {
            height: 100,
            width: 70,
            type: 'radialBar',
        },
        plotOptions: {
            radialBar: {
                hollow: {
                    size: '50%',
                },
                dataLabels: {
                    name: {
                        show: false,
                        color: '#fff'
                    },
                    value: {
                        show: true,
                        color: '#333',
                        offsetY: 5,
                        fontSize: '15px'
                    }
                }
            }
        },
        colors: ['#ecf0f4'],
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'diagonal1',
                shadeIntensity: 0.8,
                gradientToColors: ['#f56767'],
                inverseColors: false,
                opacityFrom: [1, 0.2],
                opacityTo: 1,
                stops: [0, 100],
            }
        },
        states: {
            normal: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
            hover: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
            active: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
        }
    };

    var options4 = {
        series: [<?=$active_users_percent?>],
        grid: {
            padding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
        },
        chart: {
            height: 100,
            width: 70,
            type: 'radialBar',
        },
        plotOptions: {
            radialBar: {
                hollow: {
                    size: '50%',
                },
                dataLabels: {
                    name: {
                        show: false,
                        color: '#fff'
                    },
                    value: {
                        show: true,
                        color: '#333',
                        offsetY: 5,
                        fontSize: '15px'
                    }
                }
            }
        },
        colors: ['#ecf0f4'],
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'diagonal1',
                shadeIntensity: 0.8,
                gradientToColors: ['#2979ff'],
                inverseColors: false,
                opacityFrom: [1, 0.5],
                opacityTo: 1,
                stops: [0, 100],
            }
        },
        states: {
            normal: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
            hover: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
            active: {
                filter: {
                    type: 'none',
                    value: 0,
                }
            },
        }
    };

    var chart = new ApexCharts(document.querySelector("#active_campaigns"), options);
    chart.render();

    var chart2 = new ApexCharts(document.querySelector("#active_products"), options2);
    chart2.render();

    //var chart3 = new ApexCharts(document.querySelector("#active_locations"), options3);
    //chart3.render();

    var chart4 = new ApexCharts(document.querySelector("#active_users"), options4);
    chart4.render();

</script>
<script>
    function initMap() {
        var locations = new Array();

        bounds  = new google.maps.LatLngBounds();


        <?php foreach ($locations as $locacion) { ?>

        var locacion = {
            lat: <?=($locacion->latitude) ?>,
            long: <?= ($locacion->longitude) ?>,
            visit: '<?= $this->Number->format($locacion->count_visits) ?>',
            regis: '<?= $this->Number->format($locacion->count_signin) ?>',
            last_activity: '<?= ($locacion->last_activity->i18nFormat('dd/MM/YYYY')) ?>',
            id: <?= $this->Number->format($locacion->id) ?>
        }

        locations.push([locacion.id,locacion.lat,locacion.long,locacion.visit,locacion.regis,locacion.last_activity])

        <?php } ?>

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11.5,
            center: new google.maps.LatLng(-34.6025277, -58.4136582),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        })

        google.maps.event.trigger(map, 'resize');

        var infowindow = new google.maps.InfoWindow({})

        var marker, i

        for (i = 0; i < locations.length; i++) {


            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
            })

            loc = new google.maps.LatLng(marker.position.lat(), marker.position.lng());


            bounds.extend(loc);

            var editButton = '<a href="<?= $this->Url->Build(['controller'=>'Locations', 'action' => 'view']) ?>' + '/' + locations[i][0]+'" class="btn btn-primary btn-xs">Ver ubicación</a>';

            google.maps.event.addListener(
                marker,
                'click',
                (function(marker, i) {

                    var markerContent =
                        "Visitas: " + locations[i][3]
                        + "</br>"
                        + "Registros: " + locations[i][4]
                        + "</br>"
                        + "Ultima actividad: " + locations[i][5]
                        + "</br>"
                        + "</br>"
                        + editButton;

                    return function() {
                        infowindow.setContent(markerContent)
                        infowindow.open(map, marker)
                    }
                })(marker, i)
            )

            map.fitBounds(bounds);
            map.panToBounds(bounds);
        }
    }

    <?php
    $strdata    = "";
    $strdates   = "";
    $max        = 0;
    foreach($locations_dates as $key => $value){

        if($value>0) {
            $strdata .= $value . ", ";
            $strdates .= "'" . $key . "', ";

            if ($value > $max) $max = $value;
        }
    }

    $strdata    = substr($strdata,0,strlen($strdata)-2);
    $strdates   = substr($strdates,0,strlen($strdates)-2);

    ?>

    var options = {
        series: [{
            name: 'Activadas',
            data: [<?=$strdata?>]
        }],
        chart: {
            type: 'bar',
            height: 350,
            toolbar: {
                show: false,
            }
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '25%',
                endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            type: 'string',
            categories: [<?=$strdates?>],
        },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                gradientToColors: [ '#1b00ff'],
                shadeIntensity: 1,
                type: 'horizontal',
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100, 100, 100]
            },
        },
        markers: {
            size: 4,
            colors: ["#FFA41B"],
            strokeColors: "#fff",
            strokeWidth: 2,
            hover: {
                size: 7,
            }
        },
        yaxis: {
            forceNiceScale: true,
            min: 0,
            max: <?=$max+3?>,
            title: {
                text: 'Activadas',
            },
            labels: {
                formatter: function(val) {
                    return Math.floor(val)
                }
            }
        }
    };
    var chart = new ApexCharts(document.querySelector("#ubicaciones_chart"), options);
    chart.render();


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgB2mzJi1RNCMEUrirLHuTNJ8209fbpwg&callback=initMap"></script>
