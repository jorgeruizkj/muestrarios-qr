<div class="main-container">
    <div class="pd-ltr-20">
        <div class="row">
            <div class="col-xl-6 mb-30">
                <div class="card-box height-100-p widget-style1">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h4 class="card-label">Actividad</h4>
                            </div>
                        </div>
                        <div class="bg-white pd-20 card-box mb-30">
                            <div id="productos_chart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 mb-30">
                <div class="card-box height--p widget-style1">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h4 class="card-label">Ubicaciones</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="map" style="height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-12 mb-30">
        <div class="card-box height-100-p widget-style1">
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h4 class="card-label">Locaciones</h4>
                    </div>
                </div>
                <table id="campaign-table" class="table nowrap">
                    <tbody>
                    <?php foreach ( $locationsTable  as $location ) { ?>
                        <tr>
                            <td>
                                <div style="margin: 1.5rem; margin-bottom: 2rem; font-size: 1.4rem;">
                                    <?=$location['type']?>
                                </div>
                                <div style="margin: 1.5rem; margin-bottom: -1rem; font-size: 1.01rem;">
                                    GESTOR:
                                </div>
                                <div style="margin: 1.5rem">
                                    <?=$location['username']?>
                                </div>
                                <div style="margin: 1.5rem; margin-bottom: -1rem; font-size: 1.01rem;">
                                    Última Actividad:
                                </div>
                                <div style="margin: 1.5rem">
                                    <?=$location['last_activity']?> hs.
                                </div>
                            </td>
                            <td>
                                <img src="<?= $this->Url->build('/webroot/uploads/registration/'.$location['image_route']) ?>" style="max-height: 18rem;" />
                            </td>
                            <td>
                                <div id="map<?=$location['location_id']?>" style="height:300px;width:300px"></div>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script("/webroot/deskapp2/src/plugins/apexcharts/apexcharts.min.js"); ?>
<script>
    function initMap() {
        var locations = new Array();

        bounds  = new google.maps.LatLngBounds();

        <?php foreach ($locations_product as $locacion) { ?>

        var locacion = {
            lat: <?=($locacion->latitude) ?>,
            long: <?= ($locacion->longitude) ?>,
            id: <?= $this->Number->format($locacion->id) ?>
        };

        locations.push([locacion.id,locacion.lat,locacion.long]);

        <?php } ?>

        <?php foreach ($locationsTable as $location) { ?>

        var mapName = 'map' + <?=$location['location_id']?>;

        var mapIndividual = new google.maps.Map(document.getElementById(mapName), {
            zoom: 15.5,
            center: new google.maps.LatLng(<?=$location['ubication']['latitude']?>, <?=$location['ubication']['longitude']?>),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        google.maps.event.trigger(mapIndividual, 'resize');

        var markerMapaIndividual = new google.maps.Marker({
            position: new google.maps.LatLng(<?=$location['ubication']['latitude']?>, <?=$location['ubication']['longitude']?>),
            map: mapIndividual
        });

        <?php } ?>

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11.5,
            center: new google.maps.LatLng(-34.6025277, -58.4136582),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        google.maps.event.trigger(map, 'resize');

        var marker, i;

        for (i = 0; i < locations.length; i++) {

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            loc = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

            bounds.extend(loc);
            map.fitBounds(bounds);
            map.panToBounds(bounds);
        }
    }

    <?php

    $strdataVisit    = "";
    $strdataSignin   = "";
    $strdates        = "";
    $max             = 0;

    foreach($products_dates as $key => $value) {

        if( $value['visit'] > 0 ) {

            $strdataVisit .= $value['visit'] . ", ";
            $strdates .= "'" . $key . "', ";

            if ($value['visit'] > $max) $max = $value['visit'];
        }

        if( $value['signin'] > 0 ) {

            $strdataSignin .= $value['signin'] . ", ";
            //$strdates .= "'" . $key . "', ";

            if ($value['signin'] > $max) $max = $value['signin'];
        }

    }

    $strdataVisit    = substr($strdataVisit,0,strlen($strdataVisit)-2);
    $strdataSignin   = substr($strdataSignin,0,strlen($strdataSignin)-2);
    $strdates        = substr($strdates,0,strlen($strdates)-2);

    ?>

    var options = {
        series: [
            {
                name: 'Visitas',
                data: [<?=$strdataVisit?>]
            },
            {
                name: 'Registros',
                data: [<?=$strdataSignin?>]
            }
        ],
        chart: {
            height: 350,
            type: 'bar',
            toolbar: {
                show: false
            }
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '25%',
                endingShape: 'rounded'
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: 7,
            curve: 'smooth'
        },
        xaxis: {
            type: 'string',
            categories: [<?=$strdates?>]
        },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                gradientToColors: [ '#1b00ff'],
                shadeIntensity: 1,
                type: 'horizontal',
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100, 100, 100]
            }
        },
        markers: {
            size: 4,
            colors: ["#FFA41B"],
            strokeColors: "#fff",
            strokeWidth: 2,
            hover: {size: 7}
        },
        yaxis: {
            forceNiceScale: true,
            min: 0,
            max: <?=$max+5?>,
            title: {
                text: 'Cantidades'
            },
            labels: {
                formatter: function(val) {
                    return Math.floor(val)
                }
            }
        }
    };
    var chart = new ApexCharts(document.querySelector("#productos_chart"), options);
    chart.render();
</script>
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgB2mzJi1RNCMEUrirLHuTNJ8209fbpwg&callback=initMap"></script>


