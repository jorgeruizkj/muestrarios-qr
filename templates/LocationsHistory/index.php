<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LocationsHistory[]|\Cake\Collection\CollectionInterface $locationsHistory
 */
?>
<div class="locationsHistory index content">
    <?= $this->Html->link(__('New Locations History'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Locations History') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('visit') ?></th>
                    <th><?= $this->Paginator->sort('signin') ?></th>
                    <th><?= $this->Paginator->sort('date') ?></th>
                    <th><?= $this->Paginator->sort('product_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($locationsHistory as $locationsHistory): ?>
                <tr>
                    <td><?= $this->Number->format($locationsHistory->id) ?></td>
                    <td><?= $this->Number->format($locationsHistory->visit) ?></td>
                    <td><?= $this->Number->format($locationsHistory->signin) ?></td>
                    <td><?= h($locationsHistory->date) ?></td>
                    <td><?= $locationsHistory->has('product') ? $this->Html->link($locationsHistory->product->name, ['controller' => 'Products', 'action' => 'view', $locationsHistory->product->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $locationsHistory->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $locationsHistory->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $locationsHistory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $locationsHistory->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
