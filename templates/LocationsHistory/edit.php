<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LocationsHistory $locationsHistory
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $locationsHistory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $locationsHistory->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Locations History'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="locationsHistory form content">
            <?= $this->Form->create($locationsHistory) ?>
            <fieldset>
                <legend><?= __('Edit Locations History') ?></legend>
                <?php
                    echo $this->Form->control('visit');
                    echo $this->Form->control('signin');
                    echo $this->Form->control('date', ['empty' => true]);
                    echo $this->Form->control('product_id', ['options' => $products, 'empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
