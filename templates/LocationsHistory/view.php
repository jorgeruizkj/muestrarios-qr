<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LocationsHistory $locationsHistory
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Locations History'), ['action' => 'edit', $locationsHistory->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Locations History'), ['action' => 'delete', $locationsHistory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $locationsHistory->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Locations History'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Locations History'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="locationsHistory view content">
            <h3><?= h($locationsHistory->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Product') ?></th>
                    <td><?= $locationsHistory->has('product') ? $this->Html->link($locationsHistory->product->name, ['controller' => 'Products', 'action' => 'view', $locationsHistory->product->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($locationsHistory->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Visit') ?></th>
                    <td><?= $this->Number->format($locationsHistory->visit) ?></td>
                </tr>
                <tr>
                    <th><?= __('Signin') ?></th>
                    <td><?= $this->Number->format($locationsHistory->signin) ?></td>
                </tr>
                <tr>
                    <th><?= __('Date') ?></th>
                    <td><?= h($locationsHistory->date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
