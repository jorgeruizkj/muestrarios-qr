<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LocationsProduct Entity
 *
 * @property int $id
 * @property string|null $unique_id
 * @property int|null $visit
 * @property int|null $signin
 * @property \Cake\I18n\FrozenTime|null $date
 * @property int|null $location_id
 * @property int|null $product_id
 *
 * @property \App\Model\Entity\Unique $unique
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\Product $product
 */
class LocationsProduct extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unique_id' => true,
        'visit' => true,
        'signin' => true,
        'date' => true,
        'location_id' => true,
        'product_id' => true,
        'unique' => true,
        'location' => true,
        'product' => true,
    ];
}
