<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $price
 * @property string|null $category
 * @property string|null $code
 * @property int|null $campaign_id
 * @property int|null $quantity
 * @property string $img_path
 *
 * @property \App\Model\Entity\Campaign $campaign
 * @property \App\Model\Entity\Location[] $locations
 * @property \App\Model\Entity\LocationsUser[] $locations_users
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'price' => true,
        'category' => true,
        'code' => true,
        'campaign_id' => true,
        'quantity' => true,
        'img_path' => true,
        'campaign' => true,
        'locations' => true,
        'locations_users' => true,
        'brand_id' => true, 
        'activation_count' => true,
        'activation_date' => true
    ];
}
