<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LocationsHistory Entity
 *
 * @property int $id
 * @property int|null $visit
 * @property int|null $signin
 * @property \Cake\I18n\FrozenTime|null $date
 * @property int|null $product_id
 * @property int $location_id
 *
 * @property \App\Model\Entity\Product $product
 */
class LocationsHistory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'visit' => true,
        'signin' => true,
        'date' => true,
        'product_id' => true,
        'location_id' => true,
        'product' => true,
        'last_activity' => true,
        'brand_id' => true,
        'campaign_id' => true
    ];
}
