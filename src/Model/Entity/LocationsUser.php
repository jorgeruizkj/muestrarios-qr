<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LocationsUser Entity
 *
 * @property int $id
 * @property int|null $location_id
 * @property int|null $user_id
 * @property int|null $product_id
 * @property int|null $visit
 * @property int|null $signin
 * @property \Cake\I18n\FrozenDate|null $date
 * @property int $accept_terms
 *
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Product $product
 */
class LocationsUser extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'location_id' => true,
        'user_id' => true,
        'product_id' => true,
        'visit' => true,
        'signin' => true,
        'date' => true,
        'accept_terms' => true,
        'location' => true,
        'user' => true,
        'product' => true,
        'last_activity' => true
    ];
}
