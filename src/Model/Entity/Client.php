<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Authentication\IdentityInterface;
use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;


/**
 * Client Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $username
 * @property string|null $password
 * @property string|null $dni
 * @property string|null $mail
 * @property int|null $brand_id
 * @property string $type
 * @property int $accept_terms
 *
 * @property \App\Model\Entity\Brand $brand
 */
class Client extends Entity implements IdentityInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'surname' => true,
        'username' => true,
        'password' => true,
        'dni' => true,
        'mail' => true,
        'brand_id' => true,
        'type' => true,
        'accept_terms' => true,
        'brand' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

//    protected function _setPassword(string $password) : ?string
//    {
//        if (strlen($password) > 0) {
//            $password = md5($password);
//            return $password;
//        }
//    }

    /**
     * Authentication\IdentityInterface method
     */
    public function getIdentifier()
    {
        return $this->id;
    }

    /**
     * Authentication\IdentityInterface method
     */
    public function getOriginalData()
    {
        return $this;
    }


    protected function _setPassword(string $password) : ?string
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher())->hash($password);
        }
    }



}
