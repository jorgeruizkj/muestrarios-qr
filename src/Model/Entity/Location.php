<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Location Entity
 *
 * @property int $id
 * @property string|null $latitude
 * @property string|null $longitude
 * @property int|null $visit
 * @property int|null $signin
 * @property \Cake\I18n\FrozenTime|null $date
 * @property int|null $product_id
 * @property string|null $cuit
 * @property string|null $name
 * @property string|null $address
 * @property string|null $owner_name
 * @property string|null $type
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\User[] $users
 */
class Location extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'latitude' => true,
        'longitude' => true,
        'visit' => true,
        'signin' => true,
        'date' => true,
//        'product_id' => true,
        'cuit' => true,
        'name' => true,
        'address' => true,
        'owner_name' => true,
        'type' => true,
        'product' => true,
        'users' => true,
        'last_activity' => true
    ];
}
