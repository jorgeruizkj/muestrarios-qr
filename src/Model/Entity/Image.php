<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Image Entity
 *
 * @property int $id
 * @property int|null $location_user_id
 * @property string|null $route
 *
 * @property \App\Model\Entity\LocationsUser $locations_user
 */
class Image extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'location_user_id' => true,
        'route' => true,
        'locations_user' => true,
        'unique_id'      => true,
        'location_id'    => true,
        'product_id'     => true
    ];
}
