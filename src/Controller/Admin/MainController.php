<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use DateTime;
use DatePeriod;
use DateInterval;
use DateTimeZone;

/**
 * Brands Controller
 *
 * @property \App\Model\Table\BrandsTable $Brands
 * @method \App\Model\Entity\Brand[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MainController extends AppController{


    public function index()
    {
        // Cargamos los modelos
        $this->loadModel("Locations");
        $this->loadModel("Products");
        $this->loadModel("LocationsHistory");
        $this->loadModel("Brands");
        $this->loadModel("Campaigns");

        // TENEMOS UN BRAND O SOMOS SUPERUSUARIO?
        if(!isset($this->current_brand) || $this->current_brand=="" ) {

            $locations = $this->Locations->find('all',[
                                                'contain'=> ['LocationsHistory'],
                                                "order" => 'Locations.date DESC']);

        } else {

            // VAMOS A BUSCAR LAS LOCACIONES QUE PERTENECEN A ESTA MARCA NADA MAS
            $locations = $this->Locations->find('all',
                 ['conditions' => ['id IN ' =>
                     $this->LocationsHistory->find('all',['fields'=>'location_id', 'conditions' => ['brand_id' => $this->current_brandid]])],
                        'contain'=> ['LocationsHistory' => ['conditions' => ['brand_id' => $this->current_brandid]]],
                        'order' => 'Locations.date DESC' ]);
        }

        // ARMADO DE VECTOR PARA GRAFICO DE UBICACIONES ACTIVAS X 7 DIAS Y EL MAPA
        // aqui construyo un array con keys de fechas,
        // por ejemplo: ['23/02'] => 0, ['24/02'] => 0
        $now = new DateTime( "59 days ago", new DateTimeZone('America/New_York'));
        $interval = new DateInterval( 'P1D'); // 1 Day interval
        $period = new DatePeriod( $now, $interval, 59); // 7 Days
        $locations_dates = array();
        foreach( $period as $day) {
            $key = $day->format( 'd/m');
            $locations_dates[ $key ] = 0;
        }

        // aca iteramos sobre las locations
        foreach ($locations as $location){
            // me armo un key con la fecha de la location, con el mismo formato que el array de dates
            $key = ( date_format($location->date, 'd/m'));
            // si me encuentro con una key creada en el array, que me coincida con la fecha de la location, adelanto
            // para contabilizar que tengo una activacion de una ubicacion en esa fecha, esto es para el grafico de barras
            if(isset($locations_dates[$key])) $locations_dates[$key]++;

            // contadores para la locacion especifica
            $location->count_visits = 0;
            $location->count_signin = 0;
            // voy a iterar sobre la historia de la locacion
            foreach ($location->locations_history as $history){
                // por cada vuelta, voy a sumarizarle las visitas y los registros, esto es para el mapa.
                $location->count_visits += $history->visit;
                $location->count_signin += $history->signin;
            }

        }
        // FIN ARMADO DE VECTOR PARA GRAFICO DE UBICACIONES ACTIVAS X 7 DIAS Y EL MAPA

        $title = ($this->current_brand!=null?$this->current_brand." | ":"").'Campañas';
        $brand = $this->current_brand;


        // vamos a buscar las campañas, si tengo una marca busco solo las que le pertenecen
        if($this->current_brand=="" or !isset($this->current_brand)){
            $campaigns = $this->Campaigns->find('all', [
                'contain' => ['Brands', 'Products' => ["LocationsHistory" => ["Locations"]]],
                'order' => ['Brands.name']
            ]);
        } else {
            $campaigns = $this->Campaigns->find('all', [
                'contain' => ['Brands', 'Products' => ["LocationsHistory" => ["Locations"]]],
                'order' => ['Brands.name'],
                'conditions' => ['Brands.name' => $this->current_brand]
            ]);
        }

        // CONSEGUIMOS LAS METRICAS PARA EL CHART
        $active_campaigns   = 0; // CAMPAÑAS ACTIVAS
        $active_products    = 0; // PRODUCTOS ACTIVOS
        $count_products     = 0; // CANTIDAD TOTAL DE PRODUCTOS
        $active_locations   = 0; // LOCACIONES ACTIVAS
        $count_locations    = 0; // LOCACIONES POSIBLES (SUMATORIA DE CANTIDAD DE PRODUCTOS)
        $active_users       = 0; // USUARIOS ACTIVOS
        $locationsId        = []; // auxiliar para buscar si no tengo la locacion ya
        foreach ($campaigns as $campaign){

            // si la campaña esta activa (aca tenemos que agregar un control de fecha)
            if($campaign->is_activa) $active_campaigns++;

            $count_products += count($campaign->products); // sumamos la cantidad de productos en total;
            $count_activation       = 0; // INDICE PARA PORCENTAJE DE ACTIVACION
            $campaign->activation   = 0; // PROPIEDAD DEL PORCENTAJE DE ACTIVACION DE LA CAMPAÑA
            $percent_activation     = 0; // VARIABLE DEL PORCENTAJE

            // recorremos los productos
            foreach($campaign->products as $product){
                $count_locations += $product->quantity; // SUMAMOS LA CANTIDAD DE PRODUCTOS QUE TENEMOS PORQUE SON LOCACIONES POSIBLES

                //debug($product->activation_count);
                //die();

                 foreach ($product->locations_history AS $history){
                    // si no la encuentro la agrego al array y ademas lo contabilizo
                    if(!in_array($history->location_id,$locationsId)){
                        $locationsId[] = $history->location_id;
                    }
                }

                // porcentaje de activacion
                $percent_activation += round((intval($product->activation_count) * 100) / intval($product->quantity),2);
                $count_activation++;

                // si el count de locaciones del producto me da mayor a cero, esta activo.
                if(count($product->locations_history)>0) {
                    $active_products++; // adelantamos el indice
                    //$active_locations += count($locationsId); // sumamos las locaciones que tiene

                }
            }

            // escribo la propiedad de porcentaje de activacion de la campaña
            if($count_activation>0)
                $campaign->activation += round($percent_activation/$count_activation,2);
            else
                $campaign->activation += 0;

        }

        $active_locations = count($locationsId); // contador de ubicaciones activdas
        // PORCENTAJE DE CAMPAÑAS ACTIVAS
        $active_campaigns_percent = 0;
        $active_campaigns_percent += round($active_campaigns * 100 / intval($campaigns->count()),2);
        // PORCENTAJE DE PRODUCTOS ACTIVOS
        $active_products_percent = 0;
        $active_products_percent += round($active_products * 100 / intval($count_products),2);
        // PORCENTAJE DE UBICACIONES ACTIVOS
        $active_locations_percent = 0;
        $active_locations_percent += round($active_locations * 100 / intval($count_locations),2);
        // PORCENTAJE DE USUARIOS ACTIVOS
        $active_users_percent   = 0;


        $this->set(compact('locations'));
        $this->set(compact('locations_dates'));
        $this->set(compact('active_users_percent'));
        $this->set(compact('active_users'));
        $this->set(compact('active_locations'));
        $this->set(compact('active_locations_percent'));
        $this->set(compact('active_products'));
        $this->set(compact('active_products_percent'));
        $this->set(compact('active_campaigns_percent'));
        $this->set(compact('active_campaigns'));
        $this->set(compact('title'));
        $this->set(compact('campaigns'));
        $this->set(compact('brand'));
    }

}