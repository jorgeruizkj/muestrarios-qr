<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use DateTime;
use DatePeriod;
use DateInterval;
use DateTimeZone;
/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Campaigns'],
        ];
        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, ['contain' => ['LocationsHistory','LocationsUsers.Locations','LocationsUsers.Users']]);

        // armo un array para pushearle data acerca de la locación (lat, long, etc) para el mapa en el view
        $locations_product  = [];
        // en este array cargo los location_user_id de la tabla Images para luego traer las Images correspondientes (query $imagesQuery)
        $locationsUsersId   = [];
        // genero la variable que contendrá toda la data para la tabla de locaciones
        $locationsTable     = [];

        foreach ($product->locations_users as $locacion_user) {

            $locations_product[]                                    = $locacion_user->location;

            $locationsUsersId[]                                     = $locacion_user->id;

            $locationsTable[$locacion_user->location_id]        = [
                'username'              => $locacion_user->user->name . ' ' . $locacion_user->user->surname,
                'type'                  => $locacion_user->location->type,
                'last_activity'         => $locacion_user->last_activity->format('d/m/Y H:i'),
                'ubication'             => [
                    'latitude'  => $locacion_user->location->latitude,
                    'longitude' => $locacion_user->location->longitude
                ]
            ];
        }

        // cargo el modelo (la tabla) Images para traer las imágenes de los productos
        $this->loadModel('Images');

        // traigo la data de Images (campo route para obtener la foto) para cada locación
        $imagesQuery = $this->Images->find('all')->where(['location_user_id IN' => $locationsUsersId])->order('location_user_id, date DESC')->toList();

        foreach ($product->locations_users as $locacion_user) {
            $id = $locacion_user->id;

            // IMPORTANTE! con este método evito hacer un loop foreach más
            $key = array_search($id, array_column($imagesQuery,'location_user_id'));

            if (!isset($locationsTable[$locacion_user->location_id]['image_route'])) {
                // sumo finalmente campo "route" de donde derivo la imagen correspondiente al producto en cuestión
                $locationsTable[$locacion_user->location_id]['image_route'] = $imagesQuery[$key]['route'];
                $locationsTable[$locacion_user->location_id]['location_id'] = $locacion_user->location_id;
            }
        }

        //debug($locationsTable);
        //die();

        //7 => [
        //      'username' => 'Nestor David Berjano',
        //      'type' => 'Hiper-Supermercado',
        //      'last_activity' => '05/02/2021 07:45',
        //      'ubication' => [
        //          'latitude' => '-34.6321808',
        //          'longitude' => '-58.3741279',
        //          ],
        //      'image_route' => '1612529061.jpg',
        //      'location_id' => (int) 7,
        //],

        // ARMADO DE VECTOR PARA GRAFICO DE REGISTROS Y VISITAS DE UN PRODUCTO X 31 DIAS
        $now = new DateTime( "59 days ago", new DateTimeZone('America/New_York'));
        $interval = new DateInterval( 'P1D'); // 1 Day interval
        $period = new DatePeriod( $now, $interval, 59); // 30 Days

        $products_dates = array();

        // acá defino los 31 últimos días con llave tipo dia/mes y valor = 0
        foreach( $period as $day) {
            $key = $day->format('d/m');
            $products_dates[$key]['visit']  = 0;
            $products_dates[$key]['signin'] = 0;
        }

        foreach( $product->locations_history as $history ) {
            $key = date_format($history->date,'d/m');
            if ( isset($products_dates[$key]) ) {
                $products_dates[$key]['visit'] += $history->visit;
                $products_dates[$key]['signin'] += $history->signin;
            }
        }
        // FIN ARMADO DE VECTOR PARA GRAFICO DE REGISTROS Y VISITAS DE UN PRODUCTO X 31 DIAS

        $title = $product->name;
        $this->set(compact('title'));
        $this->set(compact('product'));
        $this->set(compact('locations_product'));
        $this->set(compact('products_dates'));
        $this->set(compact('locationsTable'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEmptyEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $campaigns = $this->Products->Campaigns->find('list', ['limit' => 200]);
        $this->set(compact('product', 'campaigns'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $campaigns = $this->Products->Campaigns->find('list', ['limit' => 200]);
        $this->set(compact('product', 'campaigns'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
