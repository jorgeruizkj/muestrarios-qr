<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use DateTime;
use DatePeriod;
use DateInterval;
use DateTimeZone;

/**
 * Locations Controller
 *
 * @property \App\Model\Table\LocationsTable $Locations
 * @method \App\Model\Entity\BKPLocation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LocationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products', 'LocationHistory'],
        ];
        $locations = ($this->Locations->find('all'));
//        foreach($locations as $l){
//            debug($l->LocationsHistory);
//        }
//        die();
        $this->set(compact('locations'));
    }

    /**
     * View method
     *
     * @param string|null $id Location id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // 'contain' trae las asociaciones que tiene ese modelo(tabla) con otro en un campo más de la variable
        // de esa manera yo puedo acceder a la row de ese location_id en la TBL locations_history mediante $location->locations_history (ej. línea 92)
        // traigo también la data de locations_users
        $location = $this->Locations->get($id,['contain' => ['LocationsHistory.Products', 'Users']]);

        // cargo el modelo (la tabla) Images para traer las imágenes de los productos
        $this->loadModel('Images');

        // genero la variable que contendrá toda la data para la tabla de productos
        $productsTable = [];

        // sumo los índices que serán los product_id y sumo el nombre del producto como campo dentro de dicho índice
        foreach($location->locations_history as $lh) {
            if ( !isset($productsTable[$lh['product_id']]) ) {
                $productsTable[$lh['product_id']]                   = [];
                $productsTable[$lh['product_id']]['product_name']   = $lh->product->name;
            }
        }

        // en otro array cargo los location_user_id de la tabla Images por producto
        $locationsUsersId = [];

        foreach ($location->users as $lu) {

            $locationsUsers = $lu->_joinData;

            $locationsUsersId[] = $locationsUsers->id;

            // cuando ubico el product_id en esta tabla (users) le sumo al array para la tabla los campos username, last activity
            // y locations_users_id (este último para después ubicar con ese índice la "route" de la imagen en tabla Images)
            if( isset($productsTable[$locationsUsers['product_id']]) ) {
                $productsTable[$locationsUsers['product_id']]['username']            = $lu->name . ' ' . $lu->surname;
                $productsTable[$locationsUsers['product_id']]['last_activity']       = $lu->last_activity->format('d/m/Y H:i');
                $productsTable[$locationsUsers['product_id']]['locations_users_id']  = $locationsUsers->id;
            }
        }

        // traigo la data de Images (campo route para obtener la foto) para cada producto
        $imagesQuery = $this->Images->find('all')->where(['location_user_id IN' => $locationsUsersId])->toList();

        foreach ($location->users as $lu) {

            $id         = $lu->_joinData->id;
            $productId  = $lu->_joinData->product_id;

            // IMPORTANTE! con este método evito hacer un loop foreach más
            $key = array_search($id, array_column($imagesQuery,'location_user_id'));

            // sumo finalmente campo "route" de donde derivo la imagen correspondiente al producto en cuestión
            $productsTable[$productId]['image_route'] = $imagesQuery[$key]['route'];
        }

        // ARMADO DE VECTOR PARA GRAFICO DE REGISTROS Y VISITAS DE UNA LOCACIÓN X 31 DIAS
        $now = new DateTime( "59 days ago", new DateTimeZone('America/New_York'));
        $interval = new DateInterval( 'P1D'); // 1 Day interval
        $period = new DatePeriod( $now, $interval, 59); // 30 Days

        $locations_dates = array();

        // acá defino los 31 últimos días con llave tipo dia/mes y valor = 0
        foreach( $period as $day) {
            $key = $day->format('d/m');
            $locations_dates[$key]['visit']  = 0;
            $locations_dates[$key]['signin'] = 0;
        }

        foreach( $location->locations_history as $history ) {
            $key = date_format($history->date,'d/m');
            if ( isset($locations_dates[$key]) ) {
                $locations_dates[$key]['visit'] += $history->visit;
                $locations_dates[$key]['signin'] += $history->signin;
            }
        }
        // FIN ARMADO DE VECTOR PARA GRAFICO DE REGISTROS Y VISITAS DE UNA LOCACIÓN X 31 DIAS

        $title = $this->current_brand;
        $brand = $this->current_brand;
        $this->set(compact('title'));
        $this->set(compact('brand'));
        $this->set(compact('location'));
        $this->set(compact('locations_dates'));
        $this->set(compact('productsTable'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $location = $this->Locations->newEmptyEntity();
        if ($this->request->is('post')) {
            $location = $this->Locations->patchEntity($location, $this->request->getData());
            if ($this->Locations->save($location)) {
                $this->Flash->success(__('The location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The location could not be saved. Please, try again.'));
        }
        $products = $this->Locations->Products->find('list', ['limit' => 200]);
        $users = $this->Locations->Users->find('list', ['limit' => 200]);
        $this->set(compact('location', 'products', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Location id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $location = $this->Locations->get($id, [
            'contain' => ['Users'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $location = $this->Locations->patchEntity($location, $this->request->getData());
            if ($this->Locations->save($location)) {
                $this->Flash->success(__('The location has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The location could not be saved. Please, try again.'));
        }
        $products = $this->Locations->Products->find('list', ['limit' => 200]);
        $users = $this->Locations->Users->find('list', ['limit' => 200]);
        $this->set(compact('location', 'products', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Location id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $location = $this->Locations->get($id);
        if ($this->Locations->delete($location)) {
            $this->Flash->success(__('The location has been deleted.'));
        } else {
            $this->Flash->error(__('The location could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
