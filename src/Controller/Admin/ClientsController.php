<?php
declare(strict_types=1);

namespace App\Controller\Admin;
/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClientsController extends AppController
{


    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Authentication->allowUnauthenticated(['login', 'add']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Brands'],
        ];
        $clients = $this->paginate($this->Clients);

        $this->set(compact('clients'));
    }

    /**
     * View method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $client = $this->Clients->get($id, [
            'contain' => ['Brands'],
        ]);

        $this->set(compact('client'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->setLayout('default');
        $client = $this->Clients->newEmptyEntity();
        if ($this->request->is('post')) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));

                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('The client could not be saved. Please, try again.'));
        }
        $brands = $this->Clients->Brands->find('list', ['limit' => 200]);
        $this->set(compact('client', 'brands'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $client = $this->Clients->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The client could not be saved. Please, try again.'));
        }
        $brands = $this->Clients->Brands->find('list', ['limit' => 200]);
        $this->set(compact('client', 'brands'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $client = $this->Clients->get($id);
        if ($this->Clients->delete($client)) {
            $this->Flash->success(__('The client has been deleted.'));
        } else {
            $this->Flash->error(__('The client could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        $this->viewBuilder()->setLayout('admin_login');

        if($this->request->is('post')){
            $result = $this->Authentication->getResult();

            // If the user is logged in send them away.
            if ($result->isValid()) {

                $user = $this->Authentication->getIdentity();
                $session = $this->request->getSession();
                if($user->brand_id!=null){
                    $session->write('brandid', $user->brand_id);
                    $session->write('brandname', $user->brand->name);
                } else {
                    $session->write('brandid', null);
                    $session->write('brandname', null);
                }

                return $this->redirect(['controller' => 'Main', 'action' => 'index']);
            }

            if (!$result->isValid()) {

                switch($result->getStatus()){
                    case('FAILURE_IDENTITY_NOT_FOUND'):
                        $this->Flash->error('Usuario o contraseña incorrecta.');
                        break;
                    case('FAILURE_CREDENTIALS_INVALID'):
                        $this->Flash->error('Credenciales inválidas.');
                        break;
                    case('FAILURE_CREDENTIALS_MISSING'):
                        $this->Flash->error('Credenciales vacías.');
                        break;
                    case('FAILURE_OTHER'):
                        $this->Flash->error('Hubo un error en el proceso de autenticación.');
                        break;
                }
                return $this->redirect(['controller' => 'Clients', 'action' => 'login']);
            }
        }
    }


    public function logout()
    {
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            $this->Authentication->logout();
            $session = $this->request->getSession();
            $session->write('brandid', null);
            $session->write('brandname', null);
            return $this->redirect(['controller' => 'Clients', 'action' => 'login']);
        }
    }


}
