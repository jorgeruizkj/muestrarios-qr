<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{


    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Authentication->allowUnauthenticated(['savevisita', 'getLocationId', 'add', 'confirm', 'error', 'getImages']);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Locations'],
        ]);

        $this->set(compact('user'));
    }

    //grabo una visita con latitud, longitud, y product_id una vez que un usuario abre el QR
    public function savevisita()
    {
        //Declaro la fecha para luego usarla en LocHistory.
        $hoy = date('Y-m-d');

        //aislo lo que me llegó en POST desde login.php
        $r = $this->request->getData();

        $product_code = $r['product_code'];
        $latitud      = $r['latitud'];
        $longitud     = $r['longitud'];
        $brand_id     = $r['brand_id'];

        //busco el producto cuyo codigo recibi para guardar su id, campaign_id y brand_id en TBL LOCATIONS HISTORY
        $modelProducts = $this->loadModel('Products');
        $product = $modelProducts->find('all')->where(['code' => $product_code,
                                                            'brand_id' => $brand_id])->first();
        $product_id  = $product->id;
        $campaign_id = $product->campaign_id;

        if ($latitud != null && $longitud != null) {
            //cargo modelo de Locations
            $modeloLocacion = $this->loadModel('Locations');


            //redondeo lat y long a 4 decimales, de ahi genero dos valores separados x 0.0001
            //para tener un rango especifico para cada una que luego uso en el query
            $latitud1 = round($latitud, 3)   + LAT_MARGIN;
            $latitud2 = round($latitud, 3)   - LAT_MARGIN;
            $longitud1 = round($longitud, 3) + LONG_MARGIN;
            $longitud2 = round($longitud, 3) - LONG_MARGIN;

            //query para buscar si hay match de lat long y product_id
            $locacionfinal = $modeloLocacion->find('all')
//                ->where('(latitude <= ' . $latitud1 . ' AND latitude >= ' . $latitud2 . ')
//                                       AND (longitude <= ' . $longitud1 . ' AND longitude >= ' . $longitud2 . ') AND
//                                       (product_id = "' . $product_id . '")');
                ->where('(latitude <= ' . $latitud1 . ' AND latitude >= ' . $latitud2 . ')
                                       AND (longitude <= ' . $longitud1 . ' AND longitude >= ' . $longitud2 . ')');

            //si hay, guarda en variable $locacion
            $locacion = ($locacionfinal->first());


            //si la locacion ya esta en la tabla, chequeo que la fecha sea distinta a la de hoy
            if ($locacion != null) {
                //+1 al contador de visitas
                $locacion->visit = $locacion->visit + 1;
                $locacion->last_activity = date('Y-m-d H:i:s');

                //guardo el id de la locacion existente para posterior guardado desde el login
                $location_id = $locacion->id;

                //armo json con datos para autofill en login.php
                $autofill = json_encode($locacion);
                $modeloLocacion->save($locacion);

                //Creo instancia de LocationsHistory, query para revisar si tengo grabada esta location_id.
                $locHistoryModel = $this->loadModel('LocationsHistory');
                $locHistoryQ     = $locHistoryModel->find('all')
                                                   ->where(['location_id' => $location_id,
                                                            'product_id'  => $product_id,
                                                            'date LIKE'   => $hoy.'%',
                                                            'brand_id'    => $brand_id,
                                                            'campaign_id' => $campaign_id])
                                                   ->first();
                //Si es así, +1 a 'visit'.
                if($locHistoryQ != null){
                   $locHistoryQ->visit         = $locHistoryQ->visit + 1;
                   $locHistoryQ->last_activity = date('Y-m-d h:i:s');
                   $locHistoryModel->save($locHistoryQ);
                //De lo contrario, nuevo registro en LocHistory.
                } else {
                    $visita = $locHistoryModel->newEmptyEntity();
                    $datos  = ['location_id' => $location_id,
                               'product_id'  => $product_id,
                               'brand_id'    => $brand_id,
                               'campaign_id' => $campaign_id,
                               'visit' => 1];
                    $visita = $locHistoryModel->patchEntity($visita, $datos);
                    $locHistoryModel->save($visita);
                }

                $this->loadModel('LocationsProducts');


                //Envío los datos al formulario.
                echo $autofill;
                die();

            } else {

                // armo un array para poder hacer patchentity y devuelvo el id de la locacion para posterior guardado
                $locacion = $modeloLocacion->newEmptyEntity();
                $rel = array("longitude" => $longitud, "latitude" => $latitud, "product_id" => $product_id, "visit" => 1);
                $locacion = $modeloLocacion->patchEntity($locacion, $rel);

                if ($modeloLocacion->save($locacion)) {
                    //Si guardo una nueva locación, creo un nuevo registro en LocHistory correspondiente a ella.
                    //PASAR ESTE PROCESO ENTERO AL SAVE EN $rel, USANDO LA FK. INVESTIGAR.
                    $location_id = $locacion->id;
                    $locHistoryModel = $this->loadModel('LocationsHistory');
                    $visita = $locHistoryModel->newEmptyEntity();
                    $datos  = ['location_id' => $location_id,
                               'product_id'  => $product_id,
                               'brand_id'    => $brand_id,
                               'campaign_id' => $campaign_id,
                               'visit' => 1];
                    $visita = $locHistoryModel->patchEntity($visita, $datos);
                    $locHistoryModel->save($visita);

                    //Envío los datos al formulario.
                    echo json_encode(['id' => $locacion->id]);
                    die();

                }

            }

//            MENSAJE DEPRECADO
//            $visitaSi = array("registro" => "exitoso", "visita" => "guardada");
////RESPUESTA POSTIIVA A LA VISITA COMO JSON
//            $jsonSi = json_encode($visitaSi);
//            echo($jsonSi);
//            die();
//
//        } else {
//
//            $visitaNo = array("registro" => "fallido", "visita" => "no guardada");
////RESPUESTA NEGATIVA A LA VISITA COMO JSON
//            $jsonNo = json_encode($visitaNo);
//            echo($jsonNo);
//            die();

        }


    }

    //consigo la location id a partir de latitud y longitud enviados desde login.php x AJAX
    public function getLocationId(){
        //guardo lo que me llegó x ajax
        $r = $this->request->getData();

//        $product_code = $r['product_code'];
        $latitud = $r['latitud'];
        $longitud = $r['longitud'];
//        $brand_id = $r['brand_id'];

        //busco el producto cuyo codigo recibi para guardar su id en TBL LOCATIONS
//        $modelProducts = $this->loadModel('Products');
//        $product = $modelProducts->find('all')->where(['code' => $product_code, 'brand_id' => $brand_id])->first();
//        $product_id = $product->id;

        if ($latitud != null && $longitud != null) {
            //cargo modelo de Locations
            $modeloLocacion = $this->loadModel('Locations');


            //redondeo lat y long a 3 decimales, de ahi genero dos valores separados x las globales en paths.php
            //para tener un rango especifico para cada una que luego uso en el query
            $latitud1 = round($latitud, 3)   + LAT_MARGIN;
            $latitud2 = round($latitud, 3)   - LAT_MARGIN;
            $longitud1 = round($longitud, 3) + LONG_MARGIN;
            $longitud2 = round($longitud, 3) - LONG_MARGIN;

            //query para buscar si hay match de lat long y product_id
            $locacionfinal = $modeloLocacion->find('all')
                ->where('(latitude <= ' . $latitud1 . ' AND latitude >= ' . $latitud2 . ')
                                       AND (longitude <= ' . $longitud1 . ' AND longitude >= ' . $longitud2 . ')')
                ->select(['id'])->first();
//           debug($locacionfinal);
//            die();
            if($locacionfinal != null){
                echo $locacionfinal->id;
                die();
            }

            echo '-100';
            die();

        }
    }

    //método para conseguir imágenes por location y product _id
    public function getImages()
    {

        //guardo lo que me llegó x ajax
        $r = $this->request->getData();
        $this->loadModel('Images');
        $imagesQuery = $this->Images->find('all');
        $imagesQuery->select(['route', 'unique_id'])
                    ->where(['location_id' => $r['location_id'], 'product_id' => $r['product_id']])
                    ->distinct(['unique_id'])->order(['date' => 'ASC'])->limit(4);

        $data = json_encode($imagesQuery);
        echo $data ;
        die();
    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($marca = null, $product_code = null)
    {

        $hoy = date('Y-m-d'); //declaro la fecha de hoy, la necesito más adelante

//        if($marca != null){
//            $marca = strtolower($marca); //preparo la marca para css e imagen dinamica en login.php
//        } else {
//            debug('No llegó la marca.');
//            die();
//        }
//
//        if($product_code == null){
////            $tmp1 = explode('&amp;product_code=',$_SERVER['REQUEST_URI']);
////            $product_code = $tmp1[1];
//            debug("No llegó el product code.");
//            die();
//        }
        $this->viewBuilder()->setLayout('login'); //fijo el layout (lo busco en templates/layout)

        //voy a buscar el id de la marca para poder buscar el producto adecuado (los product_code podrían repetirse
        //entre distintas marcas)
        $brandsModel = $this->loadModel('Brands');
        $brand       = $brandsModel->find('all')->where(['name' =>  ucwords($marca) ])->first();
        if($brand == null){ //si por alguna razon la marca no existe, vamos a pantalla de error
            return $this->redirect(['action' => 'error']);
        }
        $brand_id    = $brand->id;

        //voy a buscar el producto, lo voy a necesitar para el save en LocationsUsers
        $modelProducts = $this->loadModel('Products');
        $product = $modelProducts->find('all')->where(['code' => $product_code,
            'brand_id' => $brand_id])->first();



        //si el producto con el código que tenemos no corresponde a la marca que estamos usando, hubo un error
        if($product == null){
            return $this->redirect(['action' => 'error']);
        }
        //envío el product_id para poder buscar las imágenes que le corresponden
        $product_id = $product->id;

        if ($this->request->is('post')) {

            $r = $this->request->getData(); // aislo el contenido del POST
//            debug($r);
//            die();

            //chequeo en qué "envío" vino la foto y lo guardo en $foto
            ( $r['foto']['error'] == 4 ) ? $foto = $r['foto2'] : $foto = $r['foto'];

            //query para revisar si el usuario ya se registró en el pasado
            $user = $this->Users->find('all')->where(['mail' => $r['mail']])->first();

            //si encuentro un usuario ya grabado, actualizo su last_activity y su role (si este llegase a cambiar)
            //de lo contrario, creo una nueva entidad y la patcheo con los datos recibidos via POST
            if($user == null){
                $user = $this->Users->newEmptyEntity();
                $user = $this->Users->patchEntity($user, $r);
            } else {
                $user->last_activity = date('Y-m-d h:i:s');
                $user->role          = $r['role'];
            }
            if ($this->Users->save($user)) {
                //guardo el user id por si tengo que hacer un rollback (INVESTIGAR!!)
                $saved_user_id        = $user->id;

                //si guardo el usuario, entonces voy a updatear tbl locations con el registro y last_activity
                $locationsModel       = $this->loadModel('Locations');
                $location             = $locationsModel->get($r['location_id']);
                $location->signin     = $location->signin + 1;
                $location->last_activity = date('Y-m-d h:i:s');
                $location->type       = $r['type'];
//                $location->cuit       = $r['cuit'];
//                $location->owner_name = $r['nombre_responsable']; //cambiar nombre a este dato!!
//                $location->name       = $r['nombre_local']; //a este también!
//                $location->address    = $r['address'];
                if($locationsModel->save($location)){
                    //si updateo la location, guardo el registro en locations_users y locations_history

                    //declaro el modelo de LocationsHistory
                    $locHistoryModel = $this->loadModel('LocationsHistory');
                    $locHistoryQ     = $locHistoryModel->find('all')
                                                       ->where(['location_id' => $r['location_id'],
                                                                'product_id'  => $product->id,
                                                                'date LIKE'   => $hoy.'%',
                                                                'brand_id'    => $brand_id,
                                                                'campaign_id' => $product->campaign_id])
                                                       ->first();
                    if($locHistoryQ != null) {
                        $locHistoryQ->signin        = $locHistoryQ->signin + 1;
                        $locHistoryQ->last_activity = date('Y-m-d h:i:s');
                        $locHistoryModel->save($locHistoryQ);
                    } else {
                        //duda: dejo esto? o tomo como error que no haya un registro del día de hoy en vez de agregar uno nuevo?
                        $locHistoryModel = $this->loadModel('LocationsHistory');
                        $visita = $locHistoryModel->newEmptyEntity();
                        $datos  = ['location_id' => $r['location_id'],
                                   'product_id'  => $product->id,
                                   'visit'       => 1,
                                   'signin'      => 1,
                                   'brand_id'    => $brand_id,
                                   'campaign_id' => $product->campaign_id];
                        $visita = $locHistoryModel->patchEntity($visita, $datos);
                        $locHistoryModel->save($visita);

                    }

                    //chequeo de MAX signin en locations_history para ver si hay que agregar a activation_count en tbl products
                    //empiezo yendo a buscar el número de signin más alto para este producto en esta locación
                    $signinQuery = $locHistoryModel->find('all')->where([
                        'location_id' => $r['location_id'],
                        'product_id'  => $product->id,
                        'brand_id'    => $brand_id,
                        'campaign_id' => $product->campaign_id,
                        "date < '".date('Y-m-d')."'"])
                        ->order(['signin' => 'DESC']);
                    $array_query = $signinQuery->first();
                    $max_signin = $array_query['signin'];

                    //comparo el máximo signin con el signin del registro que estoy procesando ahora mismo
                    if($locHistoryQ != null && $max_signin < $locHistoryQ->signin){
                        $product->activation_count = $product->activation_count + 1;

                        if($product->activation_count == 1 ){
                            $product->activation_date = date('Y-m-d H:i:s');
                        }

                        $this->Products->save($product);
                    }
                    //fin chequeo MAX signin


                    //declaro el modelo de LocationsUsers
                    $locUsersModel = $this->loadModel('LocationsUsers');
                    //me fijo si existe un registro para este producto, con este usuario y esta locación
                    $locUsers = $locUsersModel->find('all')
                                                   ->where(['user_id'     => $saved_user_id,
                                                            'location_id' => $r['location_id'],
                                                            'product_id'  => $product->id,
                                                            ])
                                                   ->first();
                    //si el registro existe, solo aumento los contadores de visit y signin
                    if($locUsers){
                        $locUsers->visit         = $locUsers->visit + 1;
                        $locUsers->signin        = $locUsers->signin + 1;
                        $locUsers->last_activity = date('Y-m-d h:i:s');
                    } else { //si el registro no existe, creo uno nuevo y lo lleno
                        $locUsers               = $locUsersModel->newEmptyEntity();
                        $locUsers->location_id  = $r['location_id'];
                        $locUsers->user_id      = $saved_user_id;
                        $locUsers->product_id   = $product->id;
                        $locUsers->visit        = 1;
                        $locUsers->signin       = 1;
                        $locUsers->accept_terms = $r['tyc'];
                    }



                    if($locUsersModel->save($locUsers)){ //si guardo en locations_users, registro en loc_products y despues subo la imagen a images tbl

                        //si me llega un unique_id es porque el gestor reconoció su producto en el modal de imágenes
                        //en ese caso, busco en tbl locationsproducts y le sumo +1 a visit y signin y guardo el unique_id para tbl imagees
                        //de lo contrario, creo un nuevo registro en tbl locations_products
                        $locProductsModel = $this->loadModel('LocationsProducts');

                        if($r['unique_id'] != ""){
                            $locProducts = $locProductsModel->find('all')->where(['unique_id' => $r['unique_id']])->first();
                            $locProducts->visit = $locProducts->visit +1;
                            $locProducts->signin = $locProducts->signin +1;
                            $locProducts->last_activity = date('Y-m-d h:i:s');
                        } else {
                            $locProducts = $locProductsModel->newEmptyEntity();
                            $locProducts->visit         = 1;
                            $locProducts->signin        = 1;
                            $locProducts->product_id    = $product->id;
                            $locProducts->location_id   = $r['location_id'];
                            $locProducts->unique_id     = strtoupper(substr($marca, 0, 2)).'-'.$product_code.'-000'.$product->activation_count;

                        }

                        if($locProductsModel->save($locProducts)){
                            if (!empty($foto)){ //reviso las extensiones y renombro el archivo con fecha + user_id
                                $fecha = date('Y-m-d H:i:s', strtotime('now'));
                                $ext = substr(strtolower(strrchr($foto['name'], '.')), 1); //get the extension
                                $arr_ext = array('jpg', 'jpeg', 'gif', 'png', 'jfif'); //set allowed extensions

                                if(in_array($ext, $arr_ext)){
                                    $foto['name'] = time().'_'.$saved_user_id.'.'.$ext;
                                }
                            }

                            //si el move_file es exitoso, grabo en images_tbl con el id de locations_users y el route donde está guardada
                            //la imagen (post move_uploaded_file)
                            if(move_uploaded_file($foto['tmp_name'], IMG_SAVE . $foto['name'] ))
                            {
                                $modeloImg = $this->loadModel('Images');
                                $imgRegistro = $modeloImg->newEmptyEntity();
                                $imgSave = array(   "location_user_id" => $locUsers->id,
                                                    "route"            => $foto['name'],
                                                    "unique_id"        => $locProducts->unique_id,
                                                    "location_id"      => $r['location_id'],
                                                    "product_id"       => $product->id);
                                $imgRegistro = $modeloImg->patchEntity($imgRegistro, $imgSave);

                                if($modeloImg->save($modeloImg->patchEntity($imgRegistro, $imgSave))){
                                    $this->Flash->success(('The usuario has been saved.'));
                                    return $this->redirect(['prefix' => null, 'action' => 'confirm']);
                                } else {
                                    debug("bad photo save");
                                    return $this->redirect(['action' => 'error']);

                                }
                            } else {
                                debug("bad photo move");
                                return $this->redirect(['action' => 'error']);
                            }
                        }
                        debug("bad loc prod save");
                        return $this->redirect(['action' => 'error']);
                    }
                    debug("bad loc users save");
                    return $this->redirect(['action' => 'error']);
                }
                debug("bad loc save");
                    return $this->redirect(['action' => 'error']);

            }
            debug("bad user save");
            return $this->redirect(['action' => 'error']);
        }
        $locations = $this->Users->Locations->find('list', ['limit' => 200]);
//        $this->set(compact('user', 'locations', 'product_code', 'marca', 'brand_id'));
        $this->set(compact('locations', 'product_code', 'marca', 'brand_id', 'product_id'));

    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Locations'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $locations = $this->Users->Locations->find('list', ['limit' => 200]);
        $this->set(compact('user', 'locations'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function error(){
        $this->viewBuilder()->setLayout('error'); //fijo el layout

    }

    public function confirm(){
        $this->viewBuilder()->setLayout('confirm'); //fijo el layout
    }


}
