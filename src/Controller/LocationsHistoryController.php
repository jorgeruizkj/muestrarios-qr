<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * LocationsHistory Controller
 *
 * @property \App\Model\Table\LocationsHistoryTable $LocationsHistory
 * @method \App\Model\Entity\LocationsHistory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LocationsHistoryController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products'],
        ];
        $locationsHistory = $this->paginate($this->LocationsHistory);

        $this->set(compact('locationsHistory'));
    }

    /**
     * View method
     *
     * @param string|null $id Locations History id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $locationsHistory = $this->LocationsHistory->get($id, [
            'contain' => ['Products'],
        ]);

        $this->set(compact('locationsHistory'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $locationsHistory = $this->LocationsHistory->newEmptyEntity();
        if ($this->request->is('post')) {
            $locationsHistory = $this->LocationsHistory->patchEntity($locationsHistory, $this->request->getData());
            if ($this->LocationsHistory->save($locationsHistory)) {
                $this->Flash->success(__('The locations history has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The locations history could not be saved. Please, try again.'));
        }
        $products = $this->LocationsHistory->Products->find('list', ['limit' => 200]);
        $this->set(compact('locationsHistory', 'products'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Locations History id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $locationsHistory = $this->LocationsHistory->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $locationsHistory = $this->LocationsHistory->patchEntity($locationsHistory, $this->request->getData());
            if ($this->LocationsHistory->save($locationsHistory)) {
                $this->Flash->success(__('The locations history has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The locations history could not be saved. Please, try again.'));
        }
        $products = $this->LocationsHistory->Products->find('list', ['limit' => 200]);
        $this->set(compact('locationsHistory', 'products'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Locations History id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $locationsHistory = $this->LocationsHistory->get($id);
        if ($this->LocationsHistory->delete($locationsHistory)) {
            $this->Flash->success(__('The locations history has been deleted.'));
        } else {
            $this->Flash->error(__('The locations history could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
